<?php
function tentukan_nilai($number)
{
    if ($number >= 90) {
        echo "Sangat Baik <br>";
    }elseif ($number < 90 && $number >= 75) {
        echo "Baik<br>";
    }elseif ($number < 75 && $number >= 50) {
        echo "Cukup<br>";
    }else {
        echo "Kurang<br>";
    }
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>